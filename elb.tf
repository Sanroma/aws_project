resource "aws_elb" "main_elb" {
    name = "main-elb"
    subnets = [aws_subnet.public_1.id,aws_subnet.public_2.id]
    security_groups = [aws_security_group.elb_sg.id]

    listener {
      instance_port = 80
      instance_protocol = "http"
      lb_port = 80
      lb_protocol = "http"
    }

    health_check {
      healthy_threshold = 2
      unhealthy_threshold = 2
      timeout = 3
      target = "HTTP:80/"
      interval = 30
    }

    instances = [aws_instance.WP_server_1.id,aws_instance.WP_server_2.id]
    cross_zone_load_balancing = true
    connection_draining = true
    connection_draining_timeout = 300
        
    tags = {
        Name = "main_elb"
    }
    
}

