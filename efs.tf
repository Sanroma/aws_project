resource "aws_efs_file_system" "efs_wp" {
    creation_token = "efs-wp"
    performance_mode = "generalPurpose"
    throughput_mode = "bursting"

    tags = {
      Name = "efs_fo_wp"
    }
}

resource "aws_efs_mount_target" "efs_mt_target_1a" {
  file_system_id = aws_efs_file_system.efs_wp.id
  subnet_id = aws_subnet.public_1.id
  security_groups = [aws_security_group.efs_sg.id]
}

resource "aws_efs_mount_target" "efs_mt_target_1b" {
  file_system_id = aws_efs_file_system.efs_wp.id
  subnet_id = aws_subnet.public_2.id
  security_groups = [aws_security_group.efs_sg.id]
}