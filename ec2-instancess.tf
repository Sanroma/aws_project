resource "aws_instance" "WP_server_1" {
    ami = "ami-042ad9eec03638628"
    instance_type = "t2.micro"
    vpc_security_group_ids = [aws_security_group.instances_sg.id]
    subnet_id = aws_subnet.public_1.id
#   key_name = "deployer_key"
    associate_public_ip_address = true

    tags = {
    Name    = "EC2-WP-server1"
    Project = "aws homework"
  }
}

resource "aws_instance" "WP_server_2" {
    ami = "ami-042ad9eec03638628"
    instance_type = "t2.micro"
    vpc_security_group_ids = [aws_security_group.instances_sg.id]
    subnet_id = aws_subnet.public_2.id
#   key_name = "deployer_key"
    associate_public_ip_address = true

    tags = {
    Name    = "EC2-WP-server2"
    Project = "aws homework"
  }
}

#resource "aws_key_pair" "deployer" {
#  key_name   = "deployer_key"
#  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAiv3Z2lmziLWKxTf0ICx+UcbNTZMGVLb5lAccg0exXgPip7oxBa2Y7kEAadLpgPoLJn0gfbwYXrRPjiFWaj8T3BgsIbSlmV6MKAzk0LJPcQrrrf6OU8QGdwQ0ysgpVhvuPAVC+KwI03vVql8ZsCKTIWFwIlR5mLZFWaLCFvvR+BVkYRc6Ehc1J0o9134X50SjrDNR3BcgIsH8eJQBEZH+BbqbEaxmIDZfwGO+to9W+FeJOj7FMdNSjQgjHhGLCK5CtY/inzKK1lNqlO9zOW2C88HqLae/ZG+CkLUooTk0GBGp2VbF4a5Shd+s4egSg1u3s09q0X2wN1BGrJwUora3Zw== rsa-key-20220215"
#}
