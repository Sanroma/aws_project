
resource "aws_subnet" "public_1" {
  vpc_id = aws_vpc.main.id
  cidr_block = "192.168.0.0/18"
  availability_zone = "eu-central-1a"

  tags = {
    Name = "public-eu-central-1a"
    
  }

}

resource "aws_subnet" "public_2" {
  vpc_id = aws_vpc.main.id
  cidr_block = "192.168.64.0/18"
  availability_zone = "eu-central-1b"

  tags = {
    Name = "public-eu-central-1b"
    
  }

}
